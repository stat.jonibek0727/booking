from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from rest_framework.generics import get_object_or_404
from rest_framework_simplejwt.tokens import AccessToken

from .models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError, PermissionDenied
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer

class LoginSerializer(TokenObtainPairSerializer):
    def auth_validate(self, data):
        username = data.get('username')
        

        authentication_kwargs = {
            self.username_field: username,
            "password" : data['password']
        }
        
        user = authenticate(**authentication_kwargs)
        if user is not None:
            self.user = user
        else:
            raise ValidationError(
                {
                    "success" : False,
                    "message" : "Sorry, login or password you entered is incorrect. Place check and try again!"
                }
            )

    def validate(self, data):
        self.auth_validate(data)
        

        data = self.user.tokens()
        return data
   



class LoginRefresh(TokenRefreshSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        access_token_instance = AccessToken(data['access'])
        user_id = access_token_instance['user_id']
        user = get_object_or_404(User, id = user_id)
        update_last_login(None, user)
        return data

class LogoutSerializers(serializers.Serializer):
    refresh = serializers.CharField()
