from django.urls import path
from .views import LoginRefreshView, LogOutView, LoginView

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('logout/', LogOutView.as_view()),
    path('login/refresh/', LoginRefreshView.as_view()),
]
