from django.db import models
from django.contrib.auth.models import AbstractUser
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.tokens import OutstandingToken
# Create your models here.

ROLE = (
    ('admin' , 'admin'),
    ('stadion' , 'stadion'),
    ('user' , 'user')
    
)

class User(AbstractUser):
    role = models.CharField(choices=ROLE, max_length=15, null=True, blank=True)

    def delete(self, *args, **kwargs):
        OutstandingToken.objects.filter(user=self).delete()
        super(User, self).delete(*args, **kwargs)

    
    def tokens(self):
        refresh = RefreshToken.for_user(self)
        data = {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }
        return data
    
    def hashing_password(self):
        if not self.password.startswith('pbkdf2_sha256'):
            self.set_password(self.password)
    
    def save(self, *args, **kwargs):
    
        self.hashing_password()
        super(User, self).save(*args, **kwargs)
    def __str__(self):
        return self.username