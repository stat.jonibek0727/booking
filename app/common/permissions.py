from django.db.models import Q
from rest_framework_simplejwt.tokens import OutstandingToken, BlacklistedToken
from rest_framework import permissions
from .exceptions import BAD_REQUESTException
from .roles import RoleName


class IsAdmin(permissions.BasePermission):
    object_class = None
    role_method_name = RoleName.ADMIN

    def has_permission(self, request, view):
        if request.user.id is None:
            return False
        user = request.user
        if user.role == None:
            return False
        if self.role_method_name == user.role:
            try:
                token = OutstandingToken.objects.filter(
                    user=user).order_by('-id')[0]
                block = BlacklistedToken.objects.filter(token=token)
                if not block:
                    return True
                else:
                    error = {
                        'detail': f"please login again"}
                    raise BAD_REQUESTException(error)
            except OutstandingToken.DoesNotExist:
                error = {
                    'detail': f"DoesNotExist"}
                raise BAD_REQUESTException(error)
        return False
    
    
class IsStadion(permissions.BasePermission):
    object_class = None
    role_method_name = RoleName.STADION

    def has_permission(self, request, view):
        if request.user.id is None:
            return False
        user = request.user
        if user.role == None:
            return False
        if self.role_method_name == user.role:
            try:
                token = OutstandingToken.objects.filter(
                    user=user).order_by('-id')[0]
                block = BlacklistedToken.objects.filter(token=token)
                if not block:
                    return True
                else:
                    error = {
                        'detail': f"please login again"}
                    raise BAD_REQUESTException(error)
            except OutstandingToken.DoesNotExist:
                error = {
                    'detail': f"DoesNotExist"}
                raise BAD_REQUESTException(error)
        return False
    
    
class IsUser(permissions.BasePermission):
    object_class = None
    role_method_name = RoleName.USER

    def has_permission(self, request, view):
        if request.user.id is None:
            return False
        user = request.user
        if user.role == None:
            return False
        if self.role_method_name == user.role:
            try:
                token = OutstandingToken.objects.filter(
                    user=user).order_by('-id')[0]
                block = BlacklistedToken.objects.filter(token=token)
                if not block:
                    return True
                else:
                    error = {
                        'detail': f"please login again"}
                    raise BAD_REQUESTException(error)
            except OutstandingToken.DoesNotExist:
                error = {
                    'detail': f"DoesNotExist"}
                raise BAD_REQUESTException(error)
        return False
    
class IsAuthenticated(permissions.BasePermission):
    
    def has_permission(self, request, view):
        if request.user.id is None:
            return False
        user = request.user
        if user.role == None:
            return False
        try:
            token = OutstandingToken.objects.filter(
                user=user).order_by('-id')[0]
            block = BlacklistedToken.objects.filter(token=token)
            if not block:
                return True
            else:
                error = {
                    'detail': f"please login again"}
                raise BAD_REQUESTException(error)
        except OutstandingToken.DoesNotExist:
            error = {
                'detail': f"DoesNotExist"}
            raise BAD_REQUESTException(error)