from rest_framework import serializers
from .models import Stadion, Book
from app.accounts.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('name',)


class StadionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stadion
        fields = ('id', 'name', 'adress', 'contact', 'price', 'longitude', 'latitude', 'user')
        

class StadionUpdateSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Stadion
        fields = ('id', 'name', 'adress', 'contact', 'price', 'longitude', 'latitude')
        
        
class BookStadionOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = "__all__"


class StadionAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('start', 'end')


class StadionBookingSerializer(serializers.ModelSerializer):
    resident = UserSerializer(write_only=True)

    class Meta:
        model = Book
        fields = ('resident', 'start', 'end')