from django.urls import path
from .views import ( 
    StadionListCreateApiView,
    UpdateStadionApiView,
    BookStadionOwnerApiView,
    BookStadionOwnerDeleteApiView,
    StadionDeleteApiView,
    StadionUserApiView,
    StadionAvailabilityRetrieveView,
    StadionBookingAPIView
    )

urlpatterns = [
    path('stadion/', StadionListCreateApiView.as_view()),
    path('stadion/<int:id>/', UpdateStadionApiView.as_view()),
    path('book-stadion/owner/', BookStadionOwnerApiView.as_view()),
    path('book-stadion/<int:id>/delete/', BookStadionOwnerDeleteApiView.as_view()),
    path('stadion/<int:id>/delete/', StadionDeleteApiView.as_view()),
    path('stadion-all/', StadionUserApiView.as_view()),
    path('stadion/<int:pk>/book-detail/', StadionAvailabilityRetrieveView.as_view()),
    path('stadion/<int:pk>/book/', StadionBookingAPIView.as_view()),
]
