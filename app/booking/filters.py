from datetime import datetime

import django_filters
from django import forms

from .models import Stadion, Book


class StadionFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='exact')
    

    class Meta:
        model = Stadion
        fields = []


class StadionBookingFilter(django_filters.FilterSet):
    resident = django_filters.CharFilter(field_name='resident__name', lookup_expr='exact')
    stadion = django_filters.CharFilter(field_name='stadion__name', lookup_expr='exact')
    start = django_filters.CharFilter(field_name='start', lookup_expr='exact')
    end = django_filters.CharFilter(field_name='end', lookup_expr='exact')

    class Meta:
        model = Book
        fields = []