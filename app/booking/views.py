from django.shortcuts import render
from .models import Stadion, Book
from rest_framework import generics, status, filters
from .serializers import (
    StadionSerializer,
    StadionUpdateSerializer,
    BookStadionOwnerSerializer,
    StadionAvailabilitySerializer,
    StadionBookingSerializer
    )
from .filters import StadionFilter
from typing_extensions import OrderedDict
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.pagination import PageNumberPagination
from app.common.permissions import *
from rest_framework.permissions import AllowAny
from app.common.roles import RoleName
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from datetime import datetime
from app.accounts.models import User
# Create your views here.


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page', self.page.number),
            ('page_size', self.page_size),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))




class StadionListCreateApiView(APIView):
    permission_classes = [IsStadion | IsAdmin]
    serializer_class = StadionSerializer
    search_fields = ['name', 'adress']
    filterset_class = StadionFilter
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    pagination_class = LargeResultsSetPagination
    
    
    def get(self, request):
        user = self.request.user
 
        if RoleName.ADMIN == user.role:
            queryset = Stadion.objects.all()
        elif RoleName.STADION == user.role:
            queryset = Stadion.objects.filter(user = user)
        
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
    
    def post(self, request):
        user = self.request.user
        data = self.request.data
        if RoleName.STADION == user.role:
            data['user'] = request.user.id
        serializer = self.serializer_class(data = data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class UpdateStadionApiView(generics.UpdateAPIView):
    permission_classes = [IsAdmin | IsStadion]
    serializer_class = StadionUpdateSerializer
    
    def get_queryset(self):
        user = self.request.user
        
        if RoleName.ADMIN == user.role:
            queryset = Stadion.objects.all()
        elif RoleName.STADION == user.role:
            queryset = Stadion.objects.filter(user=user)
            
        return queryset

    def get_object(self):
        
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, pk=self.kwargs['id'])
        return obj
    
class StadionDeleteApiView(generics.DestroyAPIView):
    permission_classes = [IsAdmin | IsStadion]
    serializer_class = StadionSerializer
            
    def get_queryset(self):
        user = self.request.user
        if RoleName.ADMIN == user.role:
            queryset = Stadion.objects.all()
            
        elif RoleName.STADION == user.role:
            queryset = Stadion.objects.filter(user = user)
        return queryset
    
    def get_object(self):
        
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, pk=self.kwargs['id'])
        return obj
    
    

  
class BookStadionOwnerApiView(generics.ListAPIView):
    permission_classes = [IsAdmin | IsStadion]
    serializer_class = BookStadionOwnerSerializer
    pagination_class = LargeResultsSetPagination
    
    def get_queryset(self):
        user = self.request.user
        if RoleName.ADMIN == user.role:
            queryset = Book.objects.all()
            
        elif RoleName.STADION == user.role:
            queryset = Book.objects.filter(stadion__user = user)
        return queryset



class BookStadionOwnerDeleteApiView(generics.DestroyAPIView):
    permission_classes = [IsAdmin | IsStadion]
    serializer_class = BookStadionOwnerSerializer
            
    def get_queryset(self):
        user = self.request.user
        if RoleName.ADMIN == user.role:
            queryset = Book.objects.all()
            
        elif RoleName.STADION == user.role:
            queryset = Book.objects.filter(stadion__user = user)
        return queryset
    
    def get_object(self):
        
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, pk=self.kwargs['id'])
        return obj
    

class StadionUserApiView(generics.ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = StadionSerializer
    pagination_class = LargeResultsSetPagination

    
    def get_queryset(self):
        queryset = Stadion.objects.all()
        return queryset 
    

class StadionAvailabilityRetrieveView(generics.ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = StadionAvailabilitySerializer

    def get_queryset(self):
        stadion_id = self.kwargs['pk']
        curr_time = self.request.GET.get('date', datetime.now().strftime("%d-%m-%Y"))
        obj_date = datetime.strptime(curr_time, '%d-%m-%Y')
        cur = obj_date.strftime("%Y-%m-%d")
        cur_date = datetime.strptime(cur, "%Y-%m-%d")

        queryset = Book.objects.filter(
            Q(stadion_id=stadion_id),
            Q(Q(start__day=cur_date.day) | Q(end__day=cur_date.day))
        ).order_by('start')

        stadion = Stadion.objects.get(id=stadion_id)
        start = stadion.open
        end = stadion.close

        booked_list = []

        for booked in queryset:
            if booked.start.date() == cur_date.date():
                start = booked.start.time()

            if booked.end.date() == cur_date.date():
                end = booked.end.time()

            booked_list.append((start, end))

        availability_list = []
        previous_end = stadion.open

        for start, end in booked_list:

            if previous_end < start:
                availability_list.append({
                    "start": f"{curr_time} {previous_end}",
                    "end": f"{curr_time} {start}"
                })

            previous_end = end

        if previous_end < stadion.close:
            availability_list.append({
                "start": f"{curr_time} {previous_end}",
                "end": f"{curr_time} {stadion.close}"
            })

        return availability_list
    

class StadionBookingAPIView(generics.CreateAPIView):
    queryset = Book.objects.all()
    serializer_class = StadionBookingSerializer
    permission_classes = [IsUser | IsAdmin]

    def create(self, request, *args, **kwargs):
        stadion_id = self.kwargs['pk']
        user_id = request.user.id  # user's id
        start = request.data.get('start')  # time comes in str format
        end = request.data.get('end')  # time comes in str 
        print(self.get_queryset().filter(stadion_id=stadion_id).count())
        try:
            c = parse_time(start)
            d = parse_time(end)

            if self.get_queryset().filter(stadion_id=stadion_id).count() > 0:  # more than once
                result = True
                for query in self.get_queryset().filter(stadion_id=stadion_id):
                    if result:
                        a = query.start.strftime("%Y-%m-%d %H:%M:%S")  # -> 2023-06-10 12:00:00+00:00
                        b = query.end.strftime("%Y-%m-%d %H:%M:%S")

                        if (c < a and d <= a) or (b <= c and b < d):
                            result = True
                        else:
                            result = False

                if result:
                    resident = User.objects.get(id=user_id)
                    Book.objects.create(stadion_id=stadion_id, resident=resident, start=c, end=d)
                    return Response({"message": "Stadion muvaffaqiyatli band qilindi"}, status=status.HTTP_201_CREATED)

            else:  # first time
                resident = User.objects.get(id=user_id)
                Book.objects.create(stadion_id=stadion_id, resident=resident, start=c, end=d)
                return Response({"message": "Stadion muvaffaqiyatli band qilindi"}, status=status.HTTP_201_CREATED)
            return Response({
                "error": f"uzr, siz tanlagan vaqtda stadion band"
            }, status=status.HTTP_410_GONE)

        except Exception as e:
            return Response({"error": f"{e}"}, status=status.HTTP_400_BAD_REQUEST)


def parse_time(time):
    obj_date = datetime.strptime(time, '%d-%m-%Y %H:%M:%S')
    cur = obj_date.strftime("%Y-%m-%d %H:%M:%S")
    return cur