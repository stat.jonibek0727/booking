from django.db import models
from django.core.validators import FileExtensionValidator
from app.accounts.models import User
# Create your models here.

class Stadion(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='stadion')
    name = models.CharField(max_length = 255, null=True, blank=True)
    adress = models.CharField(max_length = 255, null=True, blank=True)
    contact = models.CharField(max_length = 255, null=True, blank=True)
    price = models.BigIntegerField(default=0)
    open = models.TimeField(null=True, blank=True)
    close = models.TimeField(null=True, blank=True)
    longitude = models.CharField(max_length = 50, null=True, blank=True)
    latitude = models.CharField(max_length = 50, null=True, blank=True)
    
    def __str__(self):
        return self.name
    

class Book(models.Model):
    resident = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    stadion = models.ForeignKey(Stadion, on_delete=models.SET_NULL, null=True, related_name='book')
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return f'{self.id}'